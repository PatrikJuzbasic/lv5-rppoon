﻿using System;

namespace LV5_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {

            ITheme theme = new LightTheme();
            ITheme theme1 = new LightTheme();


            ReminderNote note1 = new ReminderNote("Do you jobs", theme);
            ReminderNote note2 = new ReminderNote("Or you will get F", theme1);
            GroupNote note3 = new GroupNote("Things to do:", theme);
            note3.insert("RPPOON");
            note3.insert("SIGNALI");
            
            Notebook notes = new Notebook(theme1);
            notes.AddNote(note1);
            notes.Display();

        }
    }
}
